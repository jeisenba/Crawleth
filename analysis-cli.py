#!/usr/bin/env python3
import argparse
from analysis import main
from analysis.stats import ChurnPeriod

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Several function to analyze the dataset',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('function_id', type=int,
                        help='ID of the function to call\n1: add_geo_to_nodes\n2: print_node_geo\n3: nodes_numbers_stats'
                             '\n4: print_geo_distrib\n5: churn')
    parser.add_argument('--filenames', nargs='+', help='List of files of the dataset (or IP addresses for function ID 3)')
    parser.add_argument('--raw_data', nargs='*', help='List of raw files of the dataset (for details analysis)')
    parser.add_argument('--churn_period', dest='churn_period', type=int, default=ChurnPeriod.ONEDAY,
                        choices=list(ChurnPeriod), help='Period for Churn (1h, 2h, 4h, 8h, 24h)')
    parser.add_argument('--nth_x_axis', dest='nth_x_axis', type=int, help='Number of x axis item to display')

    args = parser.parse_args()
    main(args.function_id, args)
