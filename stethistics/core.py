from .checks import minified, verbose


class Checker:
    def __init__(self, is_quiet: bool = False, is_minified: bool = False):
        self.quiet = is_quiet
        self.minified = is_minified
        self.backend = minified if self.minified else verbose

    def message(self, message: str):
        if not self.quiet:
            print(message)

    def aliases_check(self, crawl_data: dict, threshold: int):
        """checks for nodes with too much different addresses"""
        self.message(f"checking aliases, threshold = {threshold}")
        return self.backend.raw_aliases_check(crawl_data, threshold)

    def roommates_check(self, crawl_data: dict, threshold: int):
        """checks for IP addresses with too much different nodes"""
        self.message(f"checking roommates, threshold = {threshold}")
        return self.backend.raw_roommates_check(crawl_data, threshold)

    def ip_aliases_check(self, crawl_data: dict, threshold: int):
        """checks for IP addresses with too much different nodes"""
        self.message(f"checking ip aliases, threshold = {threshold}")
        return self.backend.raw_ip_aliases_check(crawl_data, threshold)

    def subnet_check(self, crawl_data: dict, threshold: int):
        """checks for IP addresses on the same /24 subnet"""
        self.message(f"checking subnets, threshold = {threshold}")
        return self.backend.raw_subnet_check(crawl_data, threshold)

    def concentration_check(self, crawl_data: dict, threshold: int):
        """checks for grouped node addresses"""
        self.message(f"checking concentration, threshold = {threshold}")
        return self.backend.raw_concentration_check(crawl_data, threshold)

    def identical_check(self, crawl_data: dict, threshold: int):
        """checks for nodes sharing the same id"""
        self.message(f"checking nodes with identical ids, threshold = {threshold}")
        return self.backend.raw_identical_check(crawl_data, threshold)
