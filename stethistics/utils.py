def common_prefix(a: str, b: str):
    if a == b:
        return len(a)
    else:
        for i, (va, vb) in enumerate(zip(a, b)):
            if va != vb:
                return i
    return min(len(a), len(b))


def extract_prefix(hex_string: str, size: int) -> str:
    return hex(int(f'{int(hex_string, 16):0>512b}'[:size], 2))


def func(x):
    return common_prefix(f'{int(x[0][0], 16):0>512b}', f'{int(x[1][0], 16):0>512b}'), x[0], x[1]


def ids_count(crawl_data: dict):
    return sum((len(n["Seen node IDs"]) for n in crawl_data["up"]))
