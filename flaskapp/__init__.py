from flask import Flask
from flask_cors import CORS

app = Flask(__name__, static_folder='static', template_folder='templates')
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

from flaskapp import routes
