#!/usr/bin/env python3
import argparse
from anonymize_ip import anonymize

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Several function to analyze the dataset',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('filename', type=str, nargs='+', help='List of files of the dataset')

    args = parser.parse_args()
    anonymize(args.filename)
