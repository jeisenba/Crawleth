from datetime import datetime


def timedelta(start_time):
    td = (datetime.now() - start_time)
    return td.seconds / 3600 + td.days * 24
