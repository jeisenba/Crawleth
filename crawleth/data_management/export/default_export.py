import ujson as json

from . import _export
from ...config import EXPORT_NAME


def export(redis_db, logger):
    data = _export(redis_db, logger)
    export_path = EXPORT_NAME
    with open(export_path, 'w') as outfile:
        json.dump(data, outfile)
