from pathlib import Path

import ujson as json

from protocol_utils import formatted_date
from . import _export
from ...config import TMP_EXPORT_DIRECTORY


def export(redis_db, logger):
    Path(TMP_EXPORT_DIRECTORY).mkdir(parents=True, exist_ok=True)
    data = _export(redis_db, logger)
    filename = f"{formatted_date()}.json"
    export_path = f"{TMP_EXPORT_DIRECTORY}{filename}"
    with open(export_path, 'w') as outfile:
        json.dump(data, outfile)
