import subprocess

import ujson as json

from protocol_utils import formatted_date
from . import _export
from ...config import EXPORT_DIRECTORY


def export(redis_db, logger):
    data = _export(redis_db, logger)
    filename = f"{formatted_date()}.json"
    export_path = f"{EXPORT_DIRECTORY}{filename}"
    with open(export_path, 'w') as outfile:
        json.dump(data, outfile)
    subprocess.run(["zstd", export_path, "-qfo", f"{EXPORT_DIRECTORY}compressed/{filename}.zst"])
