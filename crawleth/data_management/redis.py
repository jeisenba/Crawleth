import redis

from ..config import REDIS_HOST, REDIS_PORT


def redis_init(flush=True):
    redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
    if flush:
        redis_db.flushdb()
    return redis_db
