import pickle
from typing import Tuple


def remote_to_key(remote: Tuple[str, int, int]) -> bytes:
    return pickle.dumps(remote[0] + '_' + str(remote[1]))
