# Logging
LOG_DIRECTORY = "log"
LOG_LEVEL = "DEBUG"  # Can be set to ERROR < WARNING < INFO < DEBUG < TRACE

# Connection
FROM_UDP_PORT = 50000  # UDP port
FROM_TCP_PORT = 50000  # TCP port (unused)

# Crawling
NPING_MAX = 100
PING_MAX = 750  # Number of max concurrent Ping to wait response from
PING_CHECK_INTERVAL = 0.1  # Interval (in seconds) before nodes are being pulled out of the Ping pending pool
PING_TIMEOUT = 5  # Delay (in seconds) before a Ping times out
PING_ATTEMPT = 4  # Number of Ping attempts before a node is declared down
# The null check is an automated way to end neighbors collect. If the amount of new nodes added by interval
# is below NULL_EPS for NULL_MAX consecutive intervals, no more FindNode will be send (but the processing of
# pending nodes will still continue).
# If NULL_EPS is set to 0, the crawl will most likely not end.
NULL_MAX = 20  # The number of consecutive null delta permitted
NULL_CHECK_INTERVAL = 1.0  # Interval (in seconds) to proceed the check
NULL_EPS = 16  # The amount of new nodes before an interval is considered as a non-null delta

# redis (database)
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
# Node ID (public key) serves as key to their corresponding node details, more data may be append there
# Ping token serves as key to retrieve their corresponding node ID entry
# Key of total nodes count:
TOTAL_NODES = 'total_nodes'
# Each pool stores the nodes ID (public keys) depending on their current status:
ADDED_POOL = 'added'  # all nodes
ASKED_POOL = 'asked'  # up nodes we send a FindNode to
COMPLETED_DOWN_POOL = 'completed_down'
COMPLETED_UP_POOL = 'completed_up'  # up nodes we received Neighbors (FindNode responses) from
PING_PENDING_POOL = 'ping_pending'  # nodes to send a Ping to, pending handshake
PING_WAITING_POOL = 'ping_waiting'  # nodes we send a Ping to and waiting a Pong response from, in handshake
NEIGHBORS_PENDING_POOL = 'neighbors_pending'
NEIGHBORS_WAITING_POOL = 'neighbors_waiting'
UP_POOL = 'up'  # nodes with achieved handshake
DOWN_POOL = 'down'  # nodes with PING_ATTEMPT failed handshake(s)
# Using redis you could either: node ID -> details, token -> node ID, pool -> nodes ID

# Export
TMP_EXPORT_DIRECTORY = '/tmp/crawleth_export/'
API_DIRECTORY = "api/"
API_HOURLY_EXPORT_DIRECTORY = API_DIRECTORY + "hourly/"
API__HOURLY_BACKUP_EXPORT_DIRECTORY = API_DIRECTORY + "hourly_backups/"
API_DAILY_EXPORT_DIRECTORY = API_DIRECTORY + "daily/"
EXPORT_DIRECTORY = 'stats/'
EXPORT_NAME = 'export.json'
EXPORT_POOLS = [UP_POOL, DOWN_POOL, PING_PENDING_POOL, PING_WAITING_POOL, NEIGHBORS_PENDING_POOL,
                NEIGHBORS_WAITING_POOL]  # Pools to be exported

ALIASES_THRESHOLD = 2
SUBNETS_THRESHOLD = 24
GROUPS_THRESHOLD = 18
