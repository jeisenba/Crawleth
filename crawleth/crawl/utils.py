import logging

from crawleth.crawl import Status
from crawleth.data_management.redis import redis_init


def sig(nb, frame):
    Status.get()['stopped'] = True
    print("End signal received. Closing everything properly...")


def do_export(exporter, quiet):
    if not quiet:
        print("Exporting data...")
    redis_db = redis_init(flush=False)
    exporter(redis_db, logging.getLogger())