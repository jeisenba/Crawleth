class Status:
    _instance = None
    _data = {'stopped': False, 'timers': []}

    @classmethod
    def get(cls):
        if cls._instance is None:
            cls._instance = Status()
        return cls._instance._data
