import subprocess
import sys
from datetime import datetime

import progressbar

from ..utils import timedelta


def churn(quiet, hours):
    start_time = datetime.now()
    bar = None if quiet else progressbar.ProgressBar(max_value=hours)
    if not quiet:
        bar.update(0)
    while timedelta(start_time) < hours:
        subprocess.run([sys.argv[0], '-r', '-e', 'date', '-q'])
        if bar is not None and not quiet:
            bar.update(min(round(timedelta(start_time), 2), bar.max_value))