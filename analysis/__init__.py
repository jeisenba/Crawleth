import seaborn as sns
from functools import partial
from analysis.stats import add_geo_to_nodes, nodes_numbers_stats, \
    print_node_geo, print_geo_distrib, ChurnPeriod, churn, display_churn, \
    discovered_nodes_per_sec, subnets_stats, aliases_stats, plot_nodeid_concentration, \
    nodeid_number_stats, aliases_per_as, aliases_stats2, plot_avg_nodes, \
    ip_per_as, intersect


def main(f_id, args):
    sns.set()
    f = {
        1: partial(add_geo_to_nodes, args.filenames),
        2: partial(print_node_geo, args.filenames),
        3: partial(nodes_numbers_stats, args.filenames),
        4: partial(print_geo_distrib, args.filenames),
        5: partial(churn, args.filenames, period=ChurnPeriod(args.churn_period)),
        6: partial(display_churn, args.filenames[0], args.nth_x_axis),
        7: partial(discovered_nodes_per_sec, args.filenames[0]),
        8: partial(subnets_stats, args.filenames, args.raw_data),
        9: partial(aliases_stats, args.filenames, args.raw_data),
        10: partial(aliases_per_as, args.filenames[0]),
        11: partial(nodeid_number_stats, args.filenames),
        12: partial(plot_nodeid_concentration, args.filenames),
        13: partial(aliases_stats2, args.filenames),
        14: partial(plot_avg_nodes, args.filenames[0]),
        15: partial(ip_per_as, args.filenames),
        16: partial(intersect, args.filenames, args.raw_data)
    }

    f[f_id]()

# dataset : raw_aggregated_results.json gives date (in original_filenames object) as UTC+0, in aggregated_results
# filenames it gives it as UTC+2 (Central european summer time)
# nodes_numbers_stats data/raw_results_sept/*
# add_geo_to_nodes data/raw_results_sept/*
# print_geo_distrib data/raw_results_sept/*
# churn data/raw_results_sept/* 24 -> churn on a period of 24 hours (possible values are 1,2,4,8,24)
# display_churn data/churn 15
# discovered_nodes_per_sec 2021-11-02 10:17:21.361_walle.json
# subnets_stats data/subnets/*
# aliases_stats data/aliases/*
# aliases_per_as all_aliases_22022022-19.json
# intersect 2 raw_results file to find nodes in both