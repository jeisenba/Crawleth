interval=0 #delta of time between 2 crawls


number_of_turns=0
while [[ $1 -gt $number_of_turns ]]; do
	number_of_turns=`expr $number_of_turns + 1`
	echo "Achieving crawl number $number_of_turns"
	python3 crawl.py launch_silence
	sleep $interval
done