import os

from flaskapp import app

if __name__ == '__main__':
    if not os.path.isdir("flaskapp/static"):
        print("no 'static' directory found in module 'flaskapp'")
        exit(1)
    if not os.path.islink("flaskapp/static/api"):
        print("no 'api' link found in directory 'static' of module 'flaskapp'")
        exit(1)
    if not os.path.isdir("flaskapp/static/api/hourly/latest"):
        print("no 'latest' dir found in directory 'static/api/hourly' of module 'flaskapp'")
        print("please retry after an hour of crawling")
        exit(1)
    app.run(host="0.0.0.0")
