# Crawleth : Ethereum's P2P network Crawler
  
## Principal programs

- crawleth-cli: crawler for the Ethereum's P2P network
- fronteth.py: Website + API to expose data from the crawler
- analysis-cli: Stats and graph generator
- pingu-cli: utility to ping an Ethereum node

## Typical execution

As standalone:

```shell
python crawleth-cli.py -r -e tmp -q
```

Exporting data to be used by the website and API:

```shell
python crawleth-cli.py -A
```

Website + API:

```shell
python fronteth.py
```

## Help

For more information, see the help page of the programs (--help)
